# haskcell-lib
[![Build Status](https://travis-ci.com/ignaciobll/haskcell-lib.svg?token=d4Ka9kzAxQ1egjAtRRFu&branch=master)](https://travis-ci.com/ignaciobll/haskcell-lib)

## Nix

There is a nix integration to build and develop this project.

To build:

```
nix-build -A haskcell-lib.components.library
```

And run `nix-shell` to get a development environment with `hlint` and
`haskell-language-server`. 

You should follow part of [this guide](https://input-output-hk.github.io/haskell.nix/tutorials/getting-started/)
to have a correct nix configuration, specially the cache part.

## Build
Para construir la librería:

`stack build`

## Documentación
Para generar la documentación:

`stack haddock --open`

o bien puedes visitar la documentación en:

- https://ignaciobll.github.io/haskcell-lib

## Test
Para ejecutar los tests:

`stack test`

## Ejemplos
Para ejecutar el ejemplo:

`stack exe example-expenses`
